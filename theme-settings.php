<?php

function fblike_settings($saved_settings) {
	$defaults = array(
		'fblike_picto_title' => 0,
		'fblike_picto_title_logo_slogan' => 0,
		'fblike_picto_title_logo_font' => '',
		'fblike_picto_title_slogan_font' => '',
		'fblike_picto_title_title' => 0,
		'fblike_picto_title_title_font' => '',
	);
	$settings = array_merge($defaults, $saved_settings);

	$font_list = file_scan_directory('sites/all/themes/fblike/images/fonts','.ttf$');
	$font_options = array();
	foreach($font_list as $font) {
		$font_options[$font->basename] = $font->name;
	}

	$form = array();
	$form['fblike_picto_title'] = array(
		'#type' => 'checkbox',
		'#title' => t('Enable picto-title support. (generate the logo, slogan and titles pictures)'),
		'#default_value' => $settings['fblike_picto_title'],
		'#return_value' => 1,
		'#required' => FALSE
	);
	if (count($font_options)==0) {
		$form['fblike_picto_title']['#disabled'] = TRUE;
		$form['fblike_picto_title']['#description'] = t('Before enable this option, you must upload some truetype fonts (.ttf) in the \'themes/fblike/images/fonts\' folder. Please see the README.txt in the \'themes/fblike/images/fonts\' folder.');
	} else {
		$form['fblike_picto_title_logo_slogan'] = array(
			'#type' => 'checkbox',
			'#title' => t('Generate the logo and slogan picture'),
			'#default_value' => $settings['fblike_picto_title_logo_slogan'],
			'#return_value' => 1,
			'#disabled' => ($settings['fblike_picto_title']==0)?TRUE:FALSE,
			'#required' => FALSE
		);
		$form['fblike_picto_title_logo_font'] = array(
			'#type' => 'select',
			'#title' => t('TTF Font for the logo picture'),
			'#default_value' => $settings['fblike_picto_title_logo_font'],
			'#options' => $font_options,
			'#description' => t('Choose a font into the list'),
			'#disabled' => ($settings['fblike_picto_title']==0||$settings['fblike_picto_title_logo_slogan']==0)?TRUE:FALSE,
			'#multiple' => FALSE,
			'#required' => FALSE
		);
		$form['fblike_picto_title_slogan_font'] = array(
			'#type' => 'select',
			'#title' => t('TTF Font for the slogan picture'),
			'#default_value' => $settings['fblike_picto_title_slogan_font'],
			'#options' => $font_options,
			'#description' => t('Choose a font into the list'),
			'#disabled' => ($settings['fblike_picto_title']==0||$settings['fblike_picto_title_logo_slogan']==0)?TRUE:FALSE,
			'#multiple' => FALSE,
			'#required' => FALSE
		);
		$form['fblike_picto_title_title'] = array(
			'#type' => 'checkbox',
			'#title' => t('Generate the title pictures'),
			'#default_value' => $settings['fblike_picto_title_title'],
			'#return_value' => 1,
			'#disabled' => ($settings['fblike_picto_title']==0)?TRUE:FALSE,
			'#required' => FALSE
		);
		$form['fblike_picto_title_title_font'] = array(
			'#type' => 'select',
			'#title' => t('TTF Font for the title picture'),
			'#default_value' => $settings['fblike_picto_title_title_font'],
			'#options' => $font_options,
			'#description' => t('Choose a font into the list'),
			'#disabled' => ($settings['fblike_picto_title']==0||$settings['fblike_picto_title_title']==0)?TRUE:FALSE,
			'#multiple' => FALSE,
			'#required' => FALSE
		);
	}
	return $form;
}
?>