<div class="block">
  <?php if ($block->subject!=""): ?>
  <div class="block-header">
    <h3><?php print $block->subject ?></h3>
  </div>
  <?php endif; ?>
  <div class="block-content">
    <?php print $block->content ?>
  </div>
</div>
