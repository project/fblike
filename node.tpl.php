<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
  <?php print $picture ?>
  <?php if ($page == 0): ?><h1 class="title"><a href="<?php print $node_url ?>"><?php print $title ?></a></h1><?php endif; ?>
  <span class="submitted"><?php print $submitted ?></span>
  <div class="content"><?php print $content ?></div>
  <?php if ($terms != ""): ?><div class="taxonomy-header clearfix"><?php print t('Tags'); ?></div><div class="taxonomy"><?php print $terms ?></div><?php endif; ?>
  <?php if ($links): ?><div class="links-header clearfix"><?php print t('Links'); ?></div><div class="links"><?php print $links ?></div><?php endif; ?>
</div>
