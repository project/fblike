<?php

if (is_null(theme_get_setting('fblike_picto_title'))) {
	global $theme_key;
	$defaults = array(
		'fblike_picto_title' => 0,
		'fblike_picto_title_logo_slogan' => 0,
		'fblike_picto_title_logo_font' => '',
		'fblike_picto_title_slogan_font' => '',
		'fblike_picto_title_title' => 0,
		'fblike_picto_title_title_font' => '',
	);
	variable_set(
		str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
		array_merge($defaults, theme_get_settings($theme_key))
	);
	theme_get_setting('', TRUE);
}

function fblike_regions() {
	return array(
		'left' => t('left sidebar'),
		'content' => t('content'),
		'header' => t('header'),
		'footer' => t('footer')
	);
}

function phptemplate_breadcrumb($breadcrumb) {
	$home = variable_get('site_name', 'drupal');
	$sep = ' &raquo; ';
	if (count($breadcrumb) > 1) {
		if ( (drupal_get_title() !== '') && (strstr(end($breadcrumb),drupal_get_title()) ) == FALSE) {
			$breadcrumb[] = t(drupal_get_title(), '');
		}
		return implode($sep, $breadcrumb);
	}
}

function fblike_menu_local_tasks() {
	$output = '';
	if ($primary = menu_primary_local_tasks()) {
		$output .= "<ul class=\"clearfix\">\n". $primary ."</ul>\n";
	}
	return $output;
}

function fblike_comment_wrapper($content) {
	if ($content!="") {
		$content = '<div class="comment-header">'.t('Comments').'</div>'.$content;
	}
	return theme_comment_wrapper($content);
}

function _phptemplate_variables($hook, $vars) {
	if ($hook == 'page') {
		if ($secondary = menu_secondary_local_tasks()) {
			$vars['tabs2'] = '<ul class="clearfix">'.$secondary.'</ul>';
		}
		return $vars;
	}
	return array();
}
?>