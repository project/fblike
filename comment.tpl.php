<div class="comment<?php if ($comment->status == COMMENT_NOT_PUBLISHED) print ' comment-unpublished'; ?>">
  <?php if ($picture) : ?><?php print $picture ?><?php endif; ?>
  <div class="submitted"><?php print $submitted ?><?php if ($comment->new) : ?><span class="new"> *<?php print $new ?></span><?php endif; ?></div>
  <div class="content clearfix"><?php print $content ?></div>
  <?php if ($links) : ?><div class="links_box"><?php print $links ?></div><?php endif; ?>
</div>
