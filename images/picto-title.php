<?php

$config = array(
	"logo" => array(
		"background_color" => "1E4470",
		"text_color" => "FFFFFF",
		"fixed_height" => 44,
		"font_size" => 20
	),
	"slogan" => array(
		"background_color" => "1E4470",
		"text_color" => "FFFFFF",
		"fixed_height" => 44,
		"font_size" => 14
	),
	"title" => array(
		"background_color" => "DBE3E7",
		"text_color" => "446285",
		"fixed_height" => 34,
		"font_size" => 12
	),
);

$cache_directory = 'cache';

$text = $_GET["text"];
if ($text=="") {
	$text = "Default title";
}
$text = html_entity_decode($text);
$text = strip_tags($text);
$text = stripcslashes($text);

$mode = $_GET["mode"];
if ($mode=="") {
	$mode = "title";
}
if ($mode=="test") {
	echo "PHP version : ".phpversion()."<br />";
	echo "Zend version : ".zend_version()."<br />";
	if ($check = get_extension_funcs('gd')) {
		echo "GD support is available<br />";
		if (in_array('gd_info', $check)) {
			echo "function gd_info : OK<br />";
			$gdinfo = gd_info();
			echo "GD version : ".$gdinfo['GD Version']." (".$gdinfo['FreeType Linkage'].")<br />";
			echo "FreeType Support : ".(($gdinfo['FreeType Support'])?"Enable":"Disable")."<br />";
			echo "PNG Support : ".(($gdinfo['PNG Support'])?"Enable":"Disable")."<br />";
			echo "<!--"; print_r($gdinfo); echo "-->";
		} else {
			echo "function gd_info : Not found<br />";
		}
		if (in_array('imagettfbbox', $check)) {
			echo "function imagettfbbox : OK<br />";
		} else {
			echo "function imagettfbbox : Not found<br />";
		}
		if (in_array('imagecreatetruecolor', $check)) {
			echo "function imagecreatetruecolor : OK<br />";
		} else {
			echo "function imagecreatetruecolor : Not found<br />";
		}
		if (in_array('imagecolorallocate', $check)) {
			echo "function imagecolorallocate : OK<br />";
		} else {
			echo "function imagecolorallocate : Not found<br />";
		}
		if (in_array('imagefilledrectangle', $check)) {
			echo "function imagefilledrectangle : OK<br />";
		} else {
			echo "function imagefilledrectangle : Not found<br />";
		}
		if (in_array('imagettftext', $check)) {
			echo "function imagettftext : OK<br />";
		} else {
			echo "function imagettftext : Not found<br />";
		}
		if (in_array('imagepng', $check)) {
			echo "function imagepng : OK<br />";
		} else {
			echo "function imagepng : Not found<br />";
		}
		if (in_array('imagedestroy', $check)) {
			echo "function imagedestroy : OK<br />";
		} else {
			echo "function imagedestroy : Not found<br />";
		}
	} else {
		echo "GD support is unavailable<br />";
	}
	if (is_writable($cache_directory)) {
		echo "Cache directory : OK<br />";
	} else {
		echo "Cache directory : Not found or read only<br />";
	}
	exit;
}
if (!isset($config[$mode])) {
	$mode = "title";
}
$font = $_GET["font"];
if ($font!="") {
	$config[$mode]["font_path"] = "fonts/".$font;
}

$filename = $cache_directory.'/'.md5(serialize($config[$mode]).$text).".png";
if (!file_exists($filename)) {
	$textbox = imagettfbbox ($config[$mode]["font_size"], 0, $config[$mode]["font_path"], $text);
	$image_width = $textbox[2]+20;
	$im = imagecreatetruecolor($image_width, $config[$mode]["fixed_height"]);
	$background_color = imagecolorallocate(
		$im,
		hexdec('0x'.$config[$mode]["background_color"]{0}.$config[$mode]["background_color"]{1}),
		hexdec('0x'.$config[$mode]["background_color"]{2}.$config[$mode]["background_color"]{3}),
		hexdec('0x'.$config[$mode]["background_color"]{4}.$config[$mode]["background_color"]{5})
	);
	$text_color = imagecolorallocate(
		$im,
		hexdec('0x'.$config[$mode]["text_color"]{0}.$config[$mode]["text_color"]{1}),
		hexdec('0x'.$config[$mode]["text_color"]{2}.$config[$mode]["text_color"]{3}),
		hexdec('0x'.$config[$mode]["text_color"]{4}.$config[$mode]["text_color"]{5})
	);
	imagefilledrectangle($im, 0, 0, ($image_width-1), ($config[$mode]["fixed_height"]-1), $background_color);
	imagettftext($im, $config[$mode]["font_size"], 0, 10, (($config[$mode]["fixed_height"]-$textbox[5])/2), $text_color, $config[$mode]["font_path"], $text);
	if (is_writable($cache_directory)) {
		imagepng($im, $filename);
	}
}
header("Content-type: image/png");
if (file_exists($filename)) {
	readfile($filename);
} else {
	imagepng($im);
}
if ($im) {
	imagedestroy($im);
}
?>