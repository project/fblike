
INSTALLATION:

1. Create the folder 'sites/all/themes/fblike'
2. Copy all theme files, keeping directory structure, to the folder 'sites/all/themes/fblike'
3. Enable the fblike theme in 'admin/build/themes'
4. To enable specifics theme settings, install the Theme Settings API module (http://drupal.org/project/themesettingsapi)
5. Visit 'admin/build/themes/settings/fblike' and choose specific settings

REQUIREMENT FOR SPECIFIC SETTINGS:

This script generate the logo, title and slogan images. To work properly, you need :
- install the Theme Settings API module (http://drupal.org/project/themesettingsapi)
- PHP GD2 support
- uploading some truetype (.ttf) font file into the sites/all/themes/fblike/images/fonts

MY PERSONAL SPECIFIC SETTINGS:

- logo : Cremona.ttf : Adobe Font
- slogan : GeosansLight-Oblique.ttf : http://www.dafont.com/fr/geo-sans-light.font
- title : AMERSN__.ttf : http://www.dafont.com/fr/amerika-sans.font

CACHING GENERATED IMAGES:

To enable the cache, create a writable 'cache' directory into the sites/all/themes/fblike/images directory :
> cd sites/all/themes/fblike/images
> mkdir cache
> chmod 775 cache

For checking the cache activity, refresh a page of your Drupal site and normally some files appear in the 'cache' directory.