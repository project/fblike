<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">
<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
</head>
<?php
$fblike_picto_title = theme_get_setting('fblike_picto_title');
$fblike_picto_title_logo_slogan = theme_get_setting('fblike_picto_title_logo_slogan');
$fblike_picto_title_logo_font = theme_get_setting('fblike_picto_title_logo_font');
$fblike_picto_title_slogan_font = theme_get_setting('fblike_picto_title_slogan_font');
$fblike_picto_title_title = theme_get_setting('fblike_picto_title_title');
$fblike_picto_title_title_font = theme_get_setting('fblike_picto_title_title_font');
?>

<body>
  <div id="header_bar">
    <?php if ($site_name || $logo || $site_slogan) : ?>
    <div id="header_logo">
      <a href="<?php print $base_path ?>">
        <?php if ($logo != ""): ?><img src="<?php print check_url($logo) ?>" alt="<?php print $site_name ?> logo" id="logo" /><?php endif; ?>
        <?php if ($fblike_picto_title && $fblike_picto_title_logo_slogan): ?><img src="<?php print $base_path ?>sites/all/themes/fblike/images/picto-title.php?text=<?php print urlencode($site_name) ?>&amp;font=<?php print urlencode($fblike_picto_title_logo_font) ?>&amp;mode=logo" alt="<?php print($site_name) ?>" id="logo" /><h1 style="display:none;"><?php print($site_name) ?></h1><?php else: ?><h1><?php print($site_name) ?></h1><?php endif; ?>
        <?php if ($site_slogan != ""): ?><?php if ($fblike_picto_title && $fblike_picto_title_logo_slogan): ?><img src="<?php print $base_path ?>sites/all/themes/fblike/images/picto-title.php?text=<?php print urlencode($site_slogan) ?>&amp;font=<?php print urlencode($fblike_picto_title_slogan_font) ?>&amp;mode=slogan" alt="<?php print($site_slogan) ?>" id="slogan" /><h2 style="display:none;"><?php print($site_slogan) ?></h2><?php else: ?><h2><?php print($site_slogan) ?></h2><?php endif; ?><?php endif; ?>
      </a>
    </div>
    <?php endif; ?>
    <div id="navigator" class="clearfix">
      <div class="center_box">
        <?php if (is_array($primary_links)) : ?>
        <ul>
        <?php $primary_link_first = true; foreach ($primary_links as $link_id => $link) : ?>
        <li<?php $primary_link_class = ""; if (stristr($link_id, 'active')) $primary_link_class .= "active"; if ($primary_link_first) $primary_link_class .= " first"; $primary_link_class = trim($primary_link_class); if ($primary_link_class) print ' class="'.$primary_link_class.'"'; $primary_link_first = false; ?>><?php print l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment']) ?></li>
        <?php endforeach; ?>
        </ul>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <div class="middle-container clearfix">
    <div class="content_header">
      <div class="header_image">
        <?php if ($fblike_picto_title && $fblike_picto_title_title): ?><img src="<?php print $base_path ?>sites/all/themes/fblike/images/picto-title.php?text=<?php if ($title!="") print urlencode($title); else print urlencode($site_name); ?>&amp;font=<?php print urlencode($fblike_picto_title_title_font) ?>&amp;mode=title" alt="<?php if ($title!="") print $title; else print $site_name; ?>" id="title" /><h1 style="display:none;"><?php if ($title!="") print $title; else print $site_name; ?></h1><?php else: ?><h1><?php if ($title!="") print $title; else print $site_name; ?></h1><?php endif; ?>
      </div>
      <?php if ($search_box != ""): ?><div class="search_box"><?php print $search_box ?></div><?php endif; ?>
    </div>
    <?php if ($tabs != ""): ?>
    <div id="tabs-pri"><?php print $tabs ?></div>
    <?php endif; ?>
    <?php if (isset($tabs2)): ?>
    <div id="tabs-sec"><?php print $tabs2 ?></div>
    <?php endif; ?>
    <div class="content_pad"></div>
    <div class="content_bg clearfix">
    <?php if ($sidebar_left != ""): ?>
    <div class="side_nav">
      <?php print $sidebar_left ?>
    </div>
    <?php endif; ?>
    <div class="content_global">
      <?php if ($mission != ""): ?>
      <div class="mission">
      <div class="mission-content">
      <?php print $mission ?>
      </div>
      </div>
      <?php endif; ?>
      <?php if ($breadcrumb != ""): ?>
      <div class="bread_crumbs">
      <?php print $breadcrumb ?>
      </div>
      <?php endif; ?>
      <div class="content_main">
        <?php if ($help != ""): ?>
        <div id="help"><?php print $help ?></div>
        <?php endif; ?>
        <?php if ($messages != ""): ?>
        <?php print $messages ?>
        <?php endif; ?>
        <?php print $content; ?>
        <?php // print $feed_icons; ?>
      </div>
    </div>
  </div>
</div>
<div id="footer">
  <div id="footer_content">
    <div class="left"><a href="http://www.bonvga.net/fblike">fblike</a> by <a href="http://www.bonvga.net/">bonvga</a></div>
    <?php if ($footer_message) : ?>
    <div class="right"><?php print $footer_message;?></div>
    <?php endif; ?>
  </div>
</div>
<?php print $closure;?>
</body>
</html>